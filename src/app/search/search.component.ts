import { Component, OnInit } from '@angular/core';

import { ApiCallService } from '../api-call.service';



@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  booleanValue : boolean = false;
  constructor(private api : ApiCallService) { }
  ngOnInit(): void {}
  cityName:string = "";
  displayEnteredValue(){
    this.booleanValue = true;
    this.api.getTheDetails(this.cityName).
    subscribe((item:any)=>{
      this.booleanValue = false;
      console.log(item);
    },err=>{
      console.log(err);
    })
    
  }

}
