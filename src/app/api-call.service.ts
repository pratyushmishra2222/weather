import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiCallService {

  constructor(private httpclient: HttpClient) { }

  key = "39a9a737b07b4b703e3d1cd1e231eedc"
  apiLink = "https://api.openweathermap.org/data/2.5/weather?q=";


  getTheDetails(cityname:any){
    return this.httpclient.get(this.apiLink+cityname+"&appid="+this.key);
  }
}
